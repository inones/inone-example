/**
 *
 * NOTE: This class is auto generated by the Aiinone-IDE.
 */



#include "settingsctrl.h"

//STATIC OR GLOBLE INITIAL
SettingsCtrl *SettingsCtrl::mpSelf = NULL;

SettingsCtrl::SettingsCtrl():mpActivity(NULL),mpPage(NULL)
                                    ,mpMainList(NULL)
                                    ,mpListItem(NULL)
                                    ,mpLabel1(NULL)
{}

SettingsCtrl::~SettingsCtrl()
{
    mpSelf = NULL;
}

VOID SettingsCtrl::OnPagePreCreate(BaseActivity *pActivity, Page *pPage)
{
    LT_CHECK(pPage && pActivity);
    // CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pPage->GetPageName().c_str());
    mpSelf = this;

    mpActivity = pActivity;
    mpPage = pPage;
    mpMainList = (LTScrollList *)pPage->FindControlById(ID_SETTINGS_MAINLIST);
    mpListItem = (LTListItem *)pPage->FindControlById(ID_SETTINGS_LISTITEM);
    mpLabel1 = (LTLabel *)pPage->FindControlById(ID_SETTINGS_LABEL1);

    // mpMainList = (LTScrollList *)pPage->FindControlByName(SETTINGS_MAINLIST);
    // mpListItem = (LTListItem *)pPage->FindControlByName(SETTINGS_LISTITEM);
    // mpLabel1 = (LTLabel *)pPage->FindControlByName(SETTINGS_LABEL1);

    if(mpMainList != NULL)
    {
        mpMainList->SetClickListener(this);
        mpMainList->SetLongClickListener(this);
        mpMainList->SetScrollListListener(this);
    }
    if(mpListItem != NULL)
    {
        mpListItem->SetListItemClickListener(this);
    }
}


VOID SettingsCtrl::OnClick(LTControl *pControl, WPARAM wParam, LPARAM lParam)
{
    LT_CHECK(pControl);
    // CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
    INT ictrlid = pControl->GetCtrlId();
    switch(ictrlid){
        case ID_SETTINGS_MAINLIST:
            MainList_OnClick(pControl, wParam, lParam);
        break;
        default: break;
    }
}

VOID SettingsCtrl::OnLongClick(LTControl *pControl, WPARAM wParam, LPARAM lParam)
{
    LT_CHECK(pControl);
    // CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
    INT ictrlid = pControl->GetCtrlId();
    switch(ictrlid){
        case ID_SETTINGS_MAINLIST:
            MainList_OnLongClick(pControl, wParam, lParam);
        break;
        default: break;
    }
}

INT SettingsCtrl::GetListItemCount(LTScrollList *pControl)
{
    INT iret = ERR_INVALID;
    LT_CHECK_RET(pControl, ERR_INVALID);
    // CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
    INT ictrlid = pControl->GetCtrlId();
    switch(ictrlid){
        case ID_SETTINGS_MAINLIST:
            iret = MainList_GetListItemCount(pControl);
        break;
        default: break;
    }
    return iret;
}

INT SettingsCtrl::BuildListItemData(LTScrollList *pControl, LTListItem *pListItem, INT iIndex)
{
    INT iret = ERR_INVALID;
    LT_CHECK_RET(pControl && pListItem, ERR_INVALID);
    // CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
    INT ictrlid = pControl->GetCtrlId();
    switch(ictrlid){
        case ID_SETTINGS_MAINLIST:
            iret = MainList_BuildListItemData(pControl, pListItem, iIndex);
        break;
        default: break;
    }
    return iret;
}

VOID SettingsCtrl::NotifySelectedListItem(LTScrollList *pControl, LTListItem *pListItem, INT iIndex)
{
    LT_CHECK(pControl && pListItem);
    // CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
    INT ictrlid = pControl->GetCtrlId();
    switch(ictrlid){
        case ID_SETTINGS_MAINLIST:
            MainList_NotifySelectedListItem(pControl, pListItem, iIndex);
        break;
        default: break;
    }
}

VOID SettingsCtrl::ListItemClick(LTListItem *pControl, INT iIndex, BOOL bLongClick)
{
    LT_CHECK(pControl);
    // CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
    INT ictrlid = pControl->GetCtrlId();
    switch(ictrlid){
        case ID_SETTINGS_LISTITEM:
            ListItem_ListItemClick(pControl, iIndex, bLongClick);
        break;
        default: break;
    }
}

