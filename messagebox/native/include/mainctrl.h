/**
 *
 * NOTE: This class is auto generated by the Aiinone-IDE.
 */



#ifndef __MAINCTRL_H__
#define __MAINCTRL_H__

#include "baseactivity.h"

using namespace INONEGUI_FRAMEWORK;

//Control id in the page
#define ID_MAIN_TITLE		65537
#define ID_MAIN_CONTENT		65538
#define ID_MAIN_YES		131073
#define ID_MAIN_NO		131074

//Control name in the page
#define NAME_MAIN_TITLE		"title"
#define NAME_MAIN_CONTENT		"content"
#define NAME_MAIN_YES		"yes"
#define NAME_MAIN_NO		"no"

class MainCtrl: 
			public IPageCtrl,
			public IClickListener,
			public ILongClickListener
{
	public:
            MainCtrl();
            virtual ~MainCtrl();
            VOID OnPageCreate();
            VOID OnPageDestroy();
            VOID OnPageStart();
            VOID OnPageStop();
            VOID OnPagePreCreate(BaseActivity *pActivity, Page *pPage);
            VOID OnPageTimer(UINT32 iInterval);
            INT OnPageTouchEvent(UINT iMsg, WPARAM wParam = NULL, LPARAM lParam = NULL, DWORD dwTime = NULL);
            INT OnPageKeyEvent(UINT iMsg, WPARAM wParam = NULL, LPARAM lParam = NULL, DWORD dwTime = NULL);
            VOID OnClick(LTControl *pControl, WPARAM wParam = NULL, LPARAM lParam = NULL);
            VOID OnLongClick(LTControl *pControl, WPARAM wParam = NULL, LPARAM lParam = NULL);

            VOID yes_OnClick(LTControl *pControl, WPARAM wParam = NULL, LPARAM lParam = NULL);
            VOID yes_OnLongClick(LTControl *pControl, WPARAM wParam = NULL, LPARAM lParam = NULL);
            VOID no_OnClick(LTControl *pControl, WPARAM wParam = NULL, LPARAM lParam = NULL);
            VOID no_OnLongClick(LTControl *pControl, WPARAM wParam = NULL, LPARAM lParam = NULL);
    public:
            STATIC MainCtrl *mpSelf;
            BaseActivity *mpActivity;
            Page       *mpPage;

            LTLabel *mptitle;
            LTLabel *mpcontent;
            LTButton *mpyes;
            LTButton *mpno;
};

#endif //__MAINCTRL_H__


