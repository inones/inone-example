/**
 *
 * NOTE: This class is auto generated by the Aiinone-IDE.
 */




#ifndef __MESSAGEBOXACTIVITY_H__
#define __MESSAGEBOXACTIVITY_H__

#include "baseactivity.h"

#include "mainctrl.h"

using namespace INONEGUI_FRAMEWORK;

//Application name,using it to laucher app/page
#define APP_NAME					"messagebox"

//Page layout id in the activity
#define ID_MAIN_LAYOUT				2

//Page layout string in the activity
#define NAME_MAIN_LAYOUT			"messagebox.res,2"

//Resource Pack name in the activity
#define NAME_RES_PACK			"messagebox.res,"

extern "C" LPVOID GetInstance(STRING strName);

class MessageboxActivity:public BaseActivity{
	public:
            MessageboxActivity();
            MessageboxActivity(STRING strName);
            virtual ~MessageboxActivity();
            STRING GetLayoutByID(INT iLayoutID);
            INT OnCreate(Page *pPage);
            INT OnStart(Page *pPage);
            INT OnDestroy(Page *pPage);
            INT OnStop(Page *pPage);
            VOID OnClick(LTControl *pControl, WPARAM wParam = NULL, LPARAM lParam = NULL);
            VOID OnLongClick(LTControl *pControl, WPARAM wParam = NULL, LPARAM lParam = NULL);
            INT OnEvent(Page *pPage, UINT iMsg, WPARAM wParam = NULL, LPARAM lParam = NULL,DWORD dwTime = NULL);
            VOID OnUserTimer(LPVOID pUser,UINT32 iInterval);
            INT OnPostCreate(Page *pPage);
            INT OnPostStart(Page *pPage);
            INT OnPostStop(Page *pPage);
            INT OnPostDestroy(Page *pPage);
            INT OnPostTouchEvent(Page *pPage, UINT iMsg, WPARAM wParam = NULL, LPARAM lParam = NULL, DWORD dwTime = NULL);
            INT OnPostKeyEvent(Page *pPage, UINT iMsg, WPARAM wParam = NULL, LPARAM lParam = NULL, DWORD dwTime = NULL);
            VOID OnPostUserTimer(LPVOID pUser, UINT32 iInterval);

	private:
            LTGUI_MUTEX *mpMutex;
            MainCtrl mMainCtrl;
};

#endif //__MESSAGEBOXACTIVITY_H__

