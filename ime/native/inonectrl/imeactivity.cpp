/**
 *
 * NOTE: This class is auto generated by the Aiinone-IDE.
 */


#include "imeactivity.h"

//STATIC OR GLOBLE INITIAL 
STATIC ImeActivity* mpInstance = NULL;

//FUNCTION
ImeActivity::ImeActivity()
{
    // CLog::LogU("[%s] \n",__FUNCTION__);
}

ImeActivity::ImeActivity(STRING strName):BaseActivity(strName, WINDOW_TYPE_ACTIVITY)
{

}

ImeActivity::~ImeActivity()
{
    // CLog::LogU("[%s] \n",__FUNCTION__);
}

LPVOID GetInstance(STRING strName)
{
    if(mpInstance == NULL)
    {
        mpInstance = new ImeActivity(strName);
    }
    // CLog::LogU("[%s:%s] mpInstance:%p\n",__FILE__,__FUNCTION__,mpInstance);
    return (LPVOID)mpInstance;
}

STRING ImeActivity::GetLayoutByID(INT iLayoutID)
{
    // CLog::LogU("[%s:%s] iLayoutID:%d\n",__FILE__,__FUNCTION__,iLayoutID);
    switch(iLayoutID)
    {
        case ID_MAIN_LAYOUT:
        {
            return NAME_MAIN_LAYOUT;
        }
        case ACTIVITY_LAYOUT_RESPACK:
        {
            return NAME_RES_PACK;
        }
        default: return NAME_MAIN_LAYOUT;
    }
}

INT ImeActivity::OnCreate(Page *pPage)
{
    LT_CHECK_RET(pPage,ERR_INVALID);
    INT ictrlid = pPage->GetCtrlId();
    switch(ictrlid)
    {
        case ID_MAIN_LAYOUT:
        {
            mMainCtrl.OnPagePreCreate(this, pPage);
            mMainCtrl.OnPageCreate();
            break;
        }
        default: break;
    }

    // CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pPage->GetPageName().c_str());
    return OnPostCreate(pPage);
}

INT ImeActivity::OnStart(Page *pPage)
{
    // CLog::LogU("[%s:%s] Name:%s\n",__FILE__,__FUNCTION__,pPage->GetPageName().c_str());
    LT_CHECK_RET(pPage,ERR_INVALID);
    INT ictrlid = pPage->GetCtrlId();
    switch(ictrlid)
    {
        case ID_MAIN_LAYOUT:
        {
            mMainCtrl.OnPageStart();
            break;
        }
        default: break;
    }
    return OnPostStart(pPage);
}

INT ImeActivity::OnDestroy(Page *pPage)
{
    // CLog::LogU("[%s:%s] Name:%s\n",__FILE__,__FUNCTION__,pPage->GetPageName().c_str());
    LT_CHECK_RET(pPage,ERR_INVALID);
    INT ictrlid = pPage->GetCtrlId();
    switch(ictrlid)
    {
        case ID_MAIN_LAYOUT:
        {
            mMainCtrl.OnPageDestroy();
            break;
        }
        default: break;
    }
    return OnPostDestroy(pPage);
}

INT ImeActivity::OnStop(Page *pPage)
{
    // CLog::LogU("[%s:%s] Name:%s\n",__FILE__,__FUNCTION__,pPage->GetPageName().c_str());
    LT_CHECK_RET(pPage,ERR_INVALID);
    INT ictrlid = pPage->GetCtrlId();
    switch(ictrlid)
    {
        case ID_MAIN_LAYOUT:
        {
            mMainCtrl.OnPageStop();
            break;
        }
        default: break;
    }
    return OnPostStop(pPage);
}

INT ImeActivity::OnEvent(Page *pPage, UINT iMsg, WPARAM wParam, LPARAM lParam,DWORD dwTime)
{
    LT_CHECK_RET(pPage,ERR_INVALID);
	INT iret = ERR_OK;
	INT ictrlid = pPage->GetCtrlId();
	
	// CLog::LogU("[%s:%s] iMsg:%d \n",__FILE__,__FUNCTION__,iMsg);
    switch(ictrlid)
    {
        case ID_MAIN_LAYOUT:
        {
            if (InoIsTouchMsg(iMsg)){
                iret = mMainCtrl.OnPageTouchEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostTouchEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            else if (InoIsKeyMsg(iMsg)){
                iret = mMainCtrl.OnPageKeyEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostKeyEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            break;
        }
        default: break;
    }
    return iret;
}

VOID ImeActivity::OnUserTimer(LPVOID pUser,UINT32 iInterval) 
{
    LT_CHECK(pUser);
    if (pUser != this)
    {
        ((IPageCtrl *)pUser)->OnPageTimer(iInterval);
    }
    OnPostUserTimer(pUser, iInterval);
}

VOID ImeActivity::OnClick(LTControl *pControl, WPARAM wParam, LPARAM lParam)
{
    // CLog::LogU("[%s:%s] Control Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

VOID ImeActivity::OnLongClick(LTControl *pControl, WPARAM wParam, LPARAM lParam)
{
    // CLog::LogU("[%s:%s] Control Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

