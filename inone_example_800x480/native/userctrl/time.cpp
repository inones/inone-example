/**
 *
 * NOTE: This class is auto generated by the Aiinone-IDE.
 */


#include "inone_example_800x480activity.h"
#include <time.h>

/**
 * 用户控制层使用说明
 * 通用操作流程说明：
 * 		1. 在页面中找到相应控件对象，InoneOS已经为用户准备好相应的控件指针，请在include文件夹里找到相应
 * 		   的页面控制层文件(XXXctrl.h XXX:为页面名,如页面名为main,那头文件名为:mainctrl.h);
 * 		2. 使用控件指针调用相应的函数即可，如文本控件,mplable1->SetText("hello"),详细接口参考相应的接
 * 		   口文件或用户开发文档;
 * 		3. 如需要其它更复杂操作，请参考页面控制层文件(XXXctrl.h)提供的mpActivity/mpPage/mpxxxCtrl数据
 * 		   进行组合控制;
 * 		 
 * 其它常用全局接口函数：
 *         启动应用：InoLaunchActivity(strAppName,iPageId)
 * 						strAppName	:	"应用名称"
 * 						iPageId		:	"应用内页面ID"，如不传此参数，启动应用默认页面；
 *		   发送消息：InoPostMessage(iMsg, wParam, lParam, hWnd)
 *						iMsg		:	"消息ID,自定义消息MSG_USER递增，如#define A (MSG_USER+x)"
 *						wParam		:	可以传自定义参数		
 *						lParam		：	可以传自定义参数
 *						hWnd		：	接收对象，默认为NULL传递给ActivityManage
 */


/**
 *  @brief START_TIMER(X),start X(ms) timer. 
 *		   STOP_TIMER(X), stop X(ms) timer. 
 *	@param X:interval time,unit:millisecond 
 *  @details More details
 */
#define START_TIMER(X)   	(mpActivity->StartUserTimer(this,X))
#define STOP_TIMER(X)		(mpActivity->StopUserTimer(this,X))

#define TIMER_ID_1000 1000
#define TIMER_ID_500 500

typedef struct _tag_timeItemType
{
	STRING desc;
	UINT16 value;
} TimeItemType;
TimeItemType gYearData[100];
TimeItemType gMonData[12];
TimeItemType gDayData[31];

TimeItemType gHourData[24];
TimeItemType gMinuteData[60];
TimeItemType gSecondData[60];

UINT16 gCurYear = 0;
UINT8 gCurMon = 0;
UINT8 gCurDay = 0;
UINT8 gCurHour = 0;
UINT8 gCurMinute = 0;
UINT8 gCurSecond = 0;

STATIC VOID updateTime(LTLabel *mpDateLabel, LTLabel *mpTimeLabel)
{
	time_t now;
	struct tm *w;
	time(&now);
	w = localtime(&now);
	char time[20];

	sprintf(time, "%02d:%02d:%02d", w->tm_hour, w->tm_min, w->tm_sec);
	mpTimeLabel->SetText(time);

	sprintf(time, "%d年%02d月%02d日", 1900 + w->tm_year, w->tm_mon + 1, w->tm_mday);
	mpDateLabel->SetText(time);
}

STATIC VOID setSysTime(int year, int mon, int day, int hour, int min, int sec)
{
	struct tm time;
	struct timeval tv;

	time.tm_year = year - 1900;
	time.tm_mon = mon - 1;
	time.tm_mday = day;
	time.tm_hour = hour;
	time.tm_min = min;
	time.tm_sec = sec;

	tv.tv_sec = mktime(&time);
	tv.tv_usec = 0;
	settimeofday(&tv, NULL);
}

STATIC VOID initTime() {

	char yearStr[1], monStr[1], dayStr[1];
	char hourStr[1], minuteStr[1], secondStr[1];
	UINT16 yearOffset = 2000;

	// Year
	for (int i = 0; i < 100; i++)
	{
		int year = yearOffset + i;
		sprintf(yearStr, "%04d", year);
		gYearData[i].value = year;
		gYearData[i].desc = yearStr;
	}

	// Mon
	for (int i = 0; i < 12; i++)
	{
		sprintf(monStr, "%02d", i+1);
		gMonData[i].value = i+1;
		gMonData[i].desc = monStr;
	}

	// Day
	for (int i = 0; i < 31; i++)
	{
		sprintf(dayStr, "%02d", i+1);
		gDayData[i].value = i+1;
		gDayData[i].desc = dayStr;
	}

	// Hour
	for (int i = 0; i < 24; i++)
	{
		sprintf(hourStr, "%02d", i);
		gHourData[i].value = i;
		gHourData[i].desc = hourStr;
	}

	// Minute
	for (int i = 0; i < 60; i++)
	{
		sprintf(minuteStr, "%02d", i);
		gMinuteData[i].value = i;
		gMinuteData[i].desc = minuteStr;
	}

	// Second
	for (int i = 0; i < 60; i++)
	{
		sprintf(secondStr, "%02d", i);
		gSecondData[i].value = i;
		gSecondData[i].desc = secondStr;
	}
}

/**
 *  @brief GET_RES(X),GetResById  
 *  @param X:resource id 
 *  @details More details
 */
#define GET_RES(X)          (mpActivity->GetResById(X))

/**
 *  @brief OnPageCreate,invoked when the page create
 *  
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::OnPageCreate()
{
    CLog::LogU("[%s:%s] \n",__FILE__,__FUNCTION__);
	initTime();
}

/**
 *  @brief OnPageDestroy,invoked when the page destroy
 *  
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::OnPageDestroy()
{
    CLog::LogU("[%s:%s] \n",__FILE__,__FUNCTION__);
}

/**
 *  @brief OnPageStart,invoked when the page start
 *  
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::OnPageStart()
{
    CLog::LogU("[%s:%s] \n",__FILE__,__FUNCTION__);
	START_TIMER(TIMER_ID_1000);
}

/**
 *  @brief OnPageStart,invoked when the page stop
 *  
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::OnPageStop()
{
    CLog::LogU("[%s:%s] \n",__FILE__,__FUNCTION__);
	STOP_TIMER(TIMER_ID_1000);
}

/**
 *  @brief OnPageTimer,invoked when the enable timer
 *			start timer,please START_TIMER(X), X:interval time,unit:ms
 *			stop timer,please STOP_TIMER(X), X:interval time,unit:ms
 *	@param iInterval: timer interval value unit:ms
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::OnPageTimer(UINT32 iInterval)
{
	CLog::LogU("[%s:%s] iInterval:%d\n",__FILE__,__FUNCTION__,iInterval);
	if (iInterval == TIMER_ID_1000)
	{
		updateTime(mpDateLabel, mpTimeLabel);
	}
}

/**
 *  @brief OnPageTouchEvent,invoked when the touch event
 *	
 *	@param iMsg: touch/gesture event msg
 *				 	TOUCH MSG: MSG_TOUCH_DOWN/MSG_TOUCH_UP/MSG_TOUCH_MOVE
 *					GESTURE MSG: MSG_GESTURE_UP/MSG_GESTURE_DOWN/MSG_GESTURE_LEFT/MSG_GESTURE_RIGHT
 * 			wParam: event paramter
 * 			lParam: event paramter
 * 			dwTime: event timestamp
 *  @return ERR_OK: 消息继续传递给系统控件处理
 *			ERR_OK_EVENTDONE: 消息不会继续传递给系统控件处理，用户已经处理请返回它
 *  
 *  @details More details
 */
INT TimeCtrl::OnPageTouchEvent(UINT iMsg, WPARAM wParam, LPARAM lParam, DWORD dwTime)
{
	//CLog::LogU("[%s:%s] iMsg:%d\n",__FILE__,__FUNCTION__,iMsg);
	return ERR_OK;
}

/**
 *  @brief OnPageKeyEvent,invoked when the key event
 *	
 *	@param iMsg: key event msg
 *				 	KEY MSG: MSG_KEY_DOWN/MSG_KEY_UP/MSG_KEY_LONGPRESS
 * 			wParam: event paramter KEYCODE(INO_KEY_UP/DOWN/LEFT/RIGHT/ENTER),please reference inoneguikeys.h
 * 			lParam: event paramter
 * 			dwTime: event timestamp
 *  @return ERR_OK: 消息继续传递给系统控件处理
 *			ERR_OK_EVENTDONE: 消息不会继续传递给系统控件处理，用户已经处理请返回它
 *  
 *  @details More details
 */
INT TimeCtrl::OnPageKeyEvent(UINT iMsg, WPARAM wParam, LPARAM lParam, DWORD dwTime)
{
	//CLog::LogU("[%s:%s] iMsg:%d\n",__FILE__,__FUNCTION__,iMsg);
	return ERR_OK;
}


/**
 *  @brief Btn_SetTime_OnClick,invoked when OnClick event
 *  
 *	@param (InoControl *pControl, WPARAM wParam, LPARAM lParam)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::Btn_SetTime_OnClick(InoControl *pControl, WPARAM wParam, LPARAM lParam)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
	setSysTime(gCurYear, gCurMon, gCurDay, gCurHour, gCurMinute, gCurSecond);
	updateTime(mpDateLabel, mpTimeLabel);
}

/**
 *  @brief Btn_SetTime_OnLongClick,invoked when OnLongClick event
 *  
 *	@param (InoControl *pControl, WPARAM wParam, LPARAM lParam)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::Btn_SetTime_OnLongClick(InoControl *pControl, WPARAM wParam, LPARAM lParam)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

/**
 *  @brief SelHour_OnClick,invoked when OnClick event
 *  
 *	@param (InoControl *pControl, WPARAM wParam, LPARAM lParam)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::SelHour_OnClick(InoControl *pControl, WPARAM wParam, LPARAM lParam)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

/**
 *  @brief SelHour_OnLongClick,invoked when OnLongClick event
 *  
 *	@param (InoControl *pControl, WPARAM wParam, LPARAM lParam)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::SelHour_OnLongClick(InoControl *pControl, WPARAM wParam, LPARAM lParam)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

/**
 *  @brief SelHour_GetListItemCount,invoked when GetListItemCount event
 *  
 *	@param (InoScrollList *pControl)
 *  @return Return INT
 *  
 *  @details More details
 */
INT TimeCtrl::SelHour_GetListItemCount(InoScrollList *pControl)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
	return (sizeof(gHourData) / sizeof(TimeItemType));
}

/**
 *  @brief SelHour_BuildListItemData,invoked when BuildListItemData event
 *  
 *	@param (InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
 *  @return Return INT
 *  
 *  @details More details
 */
INT TimeCtrl::SelHour_BuildListItemData(InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
    pListItem->SetText(gHourData[iIndex].desc);
}

/**
 *  @brief SelHour_NotifySelectedListItem,invoked when NotifySelectedListItem event
 *  
 *	@param (InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::SelHour_NotifySelectedListItem(InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
{
    CLog::LogU("[%s:%s] Name:%s Index:%d\n",__FILE__,__FUNCTION__,pControl->GetName().c_str(),iIndex);
	gCurHour = gHourData[iIndex].value;
}

/**
 *  @brief ListItem1_ListItemClick,invoked when ListItemClick event
 *  
 *	@param (InoListItem *pControl, INT iIndex, BOOL bLongClick)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::ListItem1_ListItemClick(InoListItem *pControl, INT iIndex, BOOL bLongClick)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

/**
 *  @brief SelMin_OnClick,invoked when OnClick event
 *  
 *	@param (InoControl *pControl, WPARAM wParam, LPARAM lParam)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::SelMin_OnClick(InoControl *pControl, WPARAM wParam, LPARAM lParam)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

/**
 *  @brief SelMin_OnLongClick,invoked when OnLongClick event
 *  
 *	@param (InoControl *pControl, WPARAM wParam, LPARAM lParam)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::SelMin_OnLongClick(InoControl *pControl, WPARAM wParam, LPARAM lParam)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

/**
 *  @brief SelMin_GetListItemCount,invoked when GetListItemCount event
 *  
 *	@param (InoScrollList *pControl)
 *  @return Return INT
 *  
 *  @details More details
 */
INT TimeCtrl::SelMin_GetListItemCount(InoScrollList *pControl)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
    return (sizeof(gMinuteData)/sizeof(TimeItemType));
}

/**
 *  @brief SelMin_BuildListItemData,invoked when BuildListItemData event
 *  
 *	@param (InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
 *  @return Return INT
 *  
 *  @details More details
 */
INT TimeCtrl::SelMin_BuildListItemData(InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
	pListItem->SetText(gMinuteData[iIndex].desc);
}

/**
 *  @brief SelMin_NotifySelectedListItem,invoked when NotifySelectedListItem event
 *  
 *	@param (InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::SelMin_NotifySelectedListItem(InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
	gCurMinute = gMinuteData[iIndex].value;
}

/**
 *  @brief ListItem2_ListItemClick,invoked when ListItemClick event
 *  
 *	@param (InoListItem *pControl, INT iIndex, BOOL bLongClick)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::ListItem2_ListItemClick(InoListItem *pControl, INT iIndex, BOOL bLongClick)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

/**
 *  @brief SelSecond_OnClick,invoked when OnClick event
 *  
 *	@param (InoControl *pControl, WPARAM wParam, LPARAM lParam)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::SelSecond_OnClick(InoControl *pControl, WPARAM wParam, LPARAM lParam)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

/**
 *  @brief SelSecond_OnLongClick,invoked when OnLongClick event
 *  
 *	@param (InoControl *pControl, WPARAM wParam, LPARAM lParam)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::SelSecond_OnLongClick(InoControl *pControl, WPARAM wParam, LPARAM lParam)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

/**
 *  @brief SelSecond_GetListItemCount,invoked when GetListItemCount event
 *  
 *	@param (InoScrollList *pControl)
 *  @return Return INT
 *  
 *  @details More details
 */
INT TimeCtrl::SelSecond_GetListItemCount(InoScrollList *pControl)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
	return (sizeof(gSecondData) / sizeof(TimeItemType));
}

/**
 *  @brief SelSecond_BuildListItemData,invoked when BuildListItemData event
 *  
 *	@param (InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
 *  @return Return INT
 *  
 *  @details More details
 */
INT TimeCtrl::SelSecond_BuildListItemData(InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
	pListItem->SetText(gSecondData[iIndex].desc);
}

/**
 *  @brief SelSecond_NotifySelectedListItem,invoked when NotifySelectedListItem event
 *  
 *	@param (InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::SelSecond_NotifySelectedListItem(InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
	gCurSecond = gSecondData[iIndex].value;
}

/**
 *  @brief ListItem3_ListItemClick,invoked when ListItemClick event
 *  
 *	@param (InoListItem *pControl, INT iIndex, BOOL bLongClick)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::ListItem3_ListItemClick(InoListItem *pControl, INT iIndex, BOOL bLongClick)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

/**
 *  @brief SelYear_OnClick,invoked when OnClick event
 *  
 *	@param (InoControl *pControl, WPARAM wParam, LPARAM lParam)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::SelYear_OnClick(InoControl *pControl, WPARAM wParam, LPARAM lParam)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

/**
 *  @brief SelYear_OnLongClick,invoked when OnLongClick event
 *  
 *	@param (InoControl *pControl, WPARAM wParam, LPARAM lParam)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::SelYear_OnLongClick(InoControl *pControl, WPARAM wParam, LPARAM lParam)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

/**
 *  @brief SelYear_GetListItemCount,invoked when GetListItemCount event
 *  
 *	@param (InoScrollList *pControl)
 *  @return Return INT
 *  
 *  @details More details
 */
INT TimeCtrl::SelYear_GetListItemCount(InoScrollList *pControl)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
    return (sizeof(gYearData)/sizeof(TimeItemType));
}

/**
 *  @brief SelYear_BuildListItemData,invoked when BuildListItemData event
 *  
 *	@param (InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
 *  @return Return INT
 *  
 *  @details More details
 */
INT TimeCtrl::SelYear_BuildListItemData(InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
    pListItem->SetText(gYearData[iIndex].desc);
}

/**
 *  @brief SelYear_NotifySelectedListItem,invoked when NotifySelectedListItem event
 *  
 *	@param (InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::SelYear_NotifySelectedListItem(InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
	gCurYear = gYearData[iIndex].value;
}

/**
 *  @brief ListItem4_ListItemClick,invoked when ListItemClick event
 *  
 *	@param (InoListItem *pControl, INT iIndex, BOOL bLongClick)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::ListItem4_ListItemClick(InoListItem *pControl, INT iIndex, BOOL bLongClick)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

/**
 *  @brief SelMon_OnClick,invoked when OnClick event
 *  
 *	@param (InoControl *pControl, WPARAM wParam, LPARAM lParam)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::SelMon_OnClick(InoControl *pControl, WPARAM wParam, LPARAM lParam)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

/**
 *  @brief SelMon_OnLongClick,invoked when OnLongClick event
 *  
 *	@param (InoControl *pControl, WPARAM wParam, LPARAM lParam)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::SelMon_OnLongClick(InoControl *pControl, WPARAM wParam, LPARAM lParam)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

/**
 *  @brief SelMon_GetListItemCount,invoked when GetListItemCount event
 *  
 *	@param (InoScrollList *pControl)
 *  @return Return INT
 *  
 *  @details More details
 */
INT TimeCtrl::SelMon_GetListItemCount(InoScrollList *pControl)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
    return (sizeof(gMonData)/sizeof(TimeItemType));
}

/**
 *  @brief SelMon_BuildListItemData,invoked when BuildListItemData event
 *  
 *	@param (InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
 *  @return Return INT
 *  
 *  @details More details
 */
INT TimeCtrl::SelMon_BuildListItemData(InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
	pListItem->SetText(gMonData[iIndex].desc);
}

/**
 *  @brief SelMon_NotifySelectedListItem,invoked when NotifySelectedListItem event
 *  
 *	@param (InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::SelMon_NotifySelectedListItem(InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
	gCurMon = gMonData[iIndex].value;
}

/**
 *  @brief ListItem5_ListItemClick,invoked when ListItemClick event
 *  
 *	@param (InoListItem *pControl, INT iIndex, BOOL bLongClick)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::ListItem5_ListItemClick(InoListItem *pControl, INT iIndex, BOOL bLongClick)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

/**
 *  @brief SelDay_OnClick,invoked when OnClick event
 *  
 *	@param (InoControl *pControl, WPARAM wParam, LPARAM lParam)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::SelDay_OnClick(InoControl *pControl, WPARAM wParam, LPARAM lParam)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

/**
 *  @brief SelDay_OnLongClick,invoked when OnLongClick event
 *  
 *	@param (InoControl *pControl, WPARAM wParam, LPARAM lParam)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::SelDay_OnLongClick(InoControl *pControl, WPARAM wParam, LPARAM lParam)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

/**
 *  @brief SelDay_GetListItemCount,invoked when GetListItemCount event
 *  
 *	@param (InoScrollList *pControl)
 *  @return Return INT
 *  
 *  @details More details
 */
INT TimeCtrl::SelDay_GetListItemCount(InoScrollList *pControl)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
    return (sizeof(gDayData)/sizeof(TimeItemType));
}

/**
 *  @brief SelDay_BuildListItemData,invoked when BuildListItemData event
 *  
 *	@param (InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
 *  @return Return INT
 *  
 *  @details More details
 */
INT TimeCtrl::SelDay_BuildListItemData(InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
    pListItem->SetText(gDayData[iIndex].desc);
}

/**
 *  @brief SelDay_NotifySelectedListItem,invoked when NotifySelectedListItem event
 *  
 *	@param (InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::SelDay_NotifySelectedListItem(InoScrollList *pControl, InoListItem *pListItem, INT iIndex)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
	gCurDay = gDayData[iIndex].value;
}

/**
 *  @brief ListItem6_ListItemClick,invoked when ListItemClick event
 *  
 *	@param (InoListItem *pControl, INT iIndex, BOOL bLongClick)
 *  @return Return VOID
 *  
 *  @details More details
 */
VOID TimeCtrl::ListItem6_ListItemClick(InoListItem *pControl, INT iIndex, BOOL bLongClick)
{
    CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

