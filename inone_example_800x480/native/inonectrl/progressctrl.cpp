/**
 *
 * NOTE: This class is auto generated by the Aiinone-IDE.
 */



#include "progressctrl.h"

//STATIC OR GLOBLE INITIAL
ProgressCtrl *ProgressCtrl::mpSelf = NULL;

ProgressCtrl::ProgressCtrl():mpActivity(NULL),mpPage(NULL)
                                    ,mpSeekBar1(NULL)
                                    ,mpCircleBar1(NULL)
                                    ,mpCircleBar2(NULL)
                                    ,mpSeekBar2(NULL)
                                    ,mpCircleBar3(NULL)
                                    ,mpLabel1(NULL)
                                    ,mpLabel2(NULL)
                                    ,mpLabel3(NULL)
{}

ProgressCtrl::~ProgressCtrl()
{
    mpSelf = NULL;
}

VOID ProgressCtrl::OnPagePreCreate(BaseActivity *pActivity, Page *pPage)
{
    LT_CHECK(pPage && pActivity);
    // CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pPage->GetPageName().c_str());
    mpSelf = this;

    mpActivity = pActivity;
    mpPage = pPage;
    mpSeekBar1 = (InoSeekBar *)pPage->FindControlById(ID_PROGRESS_SEEKBAR1);
    mpCircleBar1 = (InoSeekBar *)pPage->FindControlById(ID_PROGRESS_CIRCLEBAR1);
    mpCircleBar2 = (InoSeekBar *)pPage->FindControlById(ID_PROGRESS_CIRCLEBAR2);
    mpSeekBar2 = (InoSeekBar *)pPage->FindControlById(ID_PROGRESS_SEEKBAR2);
    mpCircleBar3 = (InoSeekBar *)pPage->FindControlById(ID_PROGRESS_CIRCLEBAR3);
    mpLabel1 = (InoLabel *)pPage->FindControlById(ID_PROGRESS_LABEL1);
    mpLabel2 = (InoLabel *)pPage->FindControlById(ID_PROGRESS_LABEL2);
    mpLabel3 = (InoLabel *)pPage->FindControlById(ID_PROGRESS_LABEL3);

    // mpSeekBar1 = (InoSeekBar *)pPage->FindControlByName(PROGRESS_SEEKBAR1);
    // mpCircleBar1 = (InoSeekBar *)pPage->FindControlByName(PROGRESS_CIRCLEBAR1);
    // mpCircleBar2 = (InoSeekBar *)pPage->FindControlByName(PROGRESS_CIRCLEBAR2);
    // mpSeekBar2 = (InoSeekBar *)pPage->FindControlByName(PROGRESS_SEEKBAR2);
    // mpCircleBar3 = (InoSeekBar *)pPage->FindControlByName(PROGRESS_CIRCLEBAR3);
    // mpLabel1 = (InoLabel *)pPage->FindControlByName(PROGRESS_LABEL1);
    // mpLabel2 = (InoLabel *)pPage->FindControlByName(PROGRESS_LABEL2);
    // mpLabel3 = (InoLabel *)pPage->FindControlByName(PROGRESS_LABEL3);

    if(mpSeekBar1 != NULL)
    {
        mpSeekBar1->SetSeekBarListener(this);
    }
    if(mpCircleBar1 != NULL)
    {
        mpCircleBar1->SetSeekBarListener(this);
    }
    if(mpCircleBar2 != NULL)
    {
        mpCircleBar2->SetSeekBarListener(this);
    }
    if(mpSeekBar2 != NULL)
    {
        mpSeekBar2->SetSeekBarListener(this);
    }
    if(mpCircleBar3 != NULL)
    {
        mpCircleBar3->SetSeekBarListener(this);
    }
}


VOID ProgressCtrl::OnSeekBarChanged(InoSeekBar *pControl, INT iMaxValue, INT iCurValue)
{
    LT_CHECK(pControl);
    // CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
    INT ictrlid = pControl->GetCtrlId();
    switch(ictrlid){
        case ID_PROGRESS_SEEKBAR1:
            SeekBar1_OnSeekBarChanged(pControl, iMaxValue, iCurValue);
        break;
        case ID_PROGRESS_CIRCLEBAR1:
            CircleBar1_OnSeekBarChanged(pControl, iMaxValue, iCurValue);
        break;
        case ID_PROGRESS_CIRCLEBAR2:
            CircleBar2_OnSeekBarChanged(pControl, iMaxValue, iCurValue);
        break;
        case ID_PROGRESS_SEEKBAR2:
            SeekBar2_OnSeekBarChanged(pControl, iMaxValue, iCurValue);
        break;
        case ID_PROGRESS_CIRCLEBAR3:
            CircleBar3_OnSeekBarChanged(pControl, iMaxValue, iCurValue);
        break;
        default: break;
    }
}

