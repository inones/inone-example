/**
 *
 * NOTE: This class is auto generated by the Aiinone-IDE.
 */


#include "inone_example_800x480activity.h"
#ifdef DENABLE_HMIRPC
#include "../rpc/uartcom/TinyFrame.cpp"
#include "../rpc/uartcom/uartcom.cpp"
#include "../rpc/hmirpc/hmirpc.cpp"
#endif

//STATIC OR GLOBLE INITIAL 
STATIC Inone_example_800x480Activity* mpInstance = NULL;

//FUNCTION
Inone_example_800x480Activity::Inone_example_800x480Activity()
{
    // CLog::LogU("[%s] \n",__FUNCTION__);
}

Inone_example_800x480Activity::Inone_example_800x480Activity(STRING strName):BaseActivity(strName, WINDOW_TYPE_ACTIVITY)
{

}

Inone_example_800x480Activity::~Inone_example_800x480Activity()
{
    // CLog::LogU("[%s] \n",__FUNCTION__);
}

LPVOID GetInstance(STRING strName)
{
    if(mpInstance == NULL)
    {
        mpInstance = new Inone_example_800x480Activity(strName);
#ifdef DENABLE_HMIRPC
        //Initial uart communication
        UartCom::GetInstance("/dev/ttyS0");
        HmiRpc::RpcInitial(mpInstance);
        UC->UartCom_AddTypeListener(EMCUCOM_TYPE_RPC, (UC_LISTENER)HmiRpc::RpcListener);
#endif
    }
    // CLog::LogU("[%s:%s] mpInstance:%p\n",__FILE__,__FUNCTION__,mpInstance);
    return (LPVOID)mpInstance;
}

STRING Inone_example_800x480Activity::GetLayoutByID(INT iLayoutID)
{
    // CLog::LogU("[%s:%s] iLayoutID:%d\n",__FILE__,__FUNCTION__,iLayoutID);
    switch(iLayoutID)
    {
        case ID_BUTTON_LAYOUT:
        {
            return NAME_BUTTON_LAYOUT;
        }
        case ID_CANVAS_LAYOUT:
        {
            return NAME_CANVAS_LAYOUT;
        }
        case ID_CASE_LAYOUT:
        {
            return NAME_CASE_LAYOUT;
        }
        case ID_EDIT_LAYOUT:
        {
            return NAME_EDIT_LAYOUT;
        }
        case ID_LIST_LAYOUT:
        {
            return NAME_LIST_LAYOUT;
        }
        case ID_MAIN_LAYOUT:
        {
            return NAME_MAIN_LAYOUT;
        }
        case ID_METER_LAYOUT:
        {
            return NAME_METER_LAYOUT;
        }
        case ID_PICTURE_LAYOUT:
        {
            return NAME_PICTURE_LAYOUT;
        }
        case ID_PROGRESS_LAYOUT:
        {
            return NAME_PROGRESS_LAYOUT;
        }
        case ID_SETTINGS_MAIN_LAYOUT:
        {
            return NAME_SETTINGS_MAIN_LAYOUT;
        }
        case ID_TEXT_LAYOUT:
        {
            return NAME_TEXT_LAYOUT;
        }
        case ID_TIME_LAYOUT:
        {
            return NAME_TIME_LAYOUT;
        }
        case ACTIVITY_LAYOUT_RESPACK:
        {
            return NAME_RES_PACK;
        }
        case ACTIVITY_LAYOUT_MAIN:
        {
            return NAME_MAIN_LAYOUT;
        }
        default: return "";
    }
}

INT Inone_example_800x480Activity::OnCreate(Page *pPage)
{
    LT_CHECK_RET(pPage,ERR_INVALID);
    INT ictrlid = pPage->GetCtrlId();
    switch(ictrlid)
    {
        case ID_BUTTON_LAYOUT:
        {
            mButtonCtrl.OnPagePreCreate(this, pPage);
            mButtonCtrl.OnPageCreate();
            break;
        }
        case ID_CANVAS_LAYOUT:
        {
            mCanvasCtrl.OnPagePreCreate(this, pPage);
            mCanvasCtrl.OnPageCreate();
            break;
        }
        case ID_CASE_LAYOUT:
        {
            mCaseCtrl.OnPagePreCreate(this, pPage);
            mCaseCtrl.OnPageCreate();
            break;
        }
        case ID_EDIT_LAYOUT:
        {
            mEditCtrl.OnPagePreCreate(this, pPage);
            mEditCtrl.OnPageCreate();
            break;
        }
        case ID_LIST_LAYOUT:
        {
            mListCtrl.OnPagePreCreate(this, pPage);
            mListCtrl.OnPageCreate();
            break;
        }
        case ID_MAIN_LAYOUT:
        {
            mMainCtrl.OnPagePreCreate(this, pPage);
            mMainCtrl.OnPageCreate();
            break;
        }
        case ID_METER_LAYOUT:
        {
            mMeterCtrl.OnPagePreCreate(this, pPage);
            mMeterCtrl.OnPageCreate();
            break;
        }
        case ID_PICTURE_LAYOUT:
        {
            mPictureCtrl.OnPagePreCreate(this, pPage);
            mPictureCtrl.OnPageCreate();
            break;
        }
        case ID_PROGRESS_LAYOUT:
        {
            mProgressCtrl.OnPagePreCreate(this, pPage);
            mProgressCtrl.OnPageCreate();
            break;
        }
        case ID_SETTINGS_MAIN_LAYOUT:
        {
            mSettings_mainCtrl.OnPagePreCreate(this, pPage);
            mSettings_mainCtrl.OnPageCreate();
            break;
        }
        case ID_TEXT_LAYOUT:
        {
            mTextCtrl.OnPagePreCreate(this, pPage);
            mTextCtrl.OnPageCreate();
            break;
        }
        case ID_TIME_LAYOUT:
        {
            mTimeCtrl.OnPagePreCreate(this, pPage);
            mTimeCtrl.OnPageCreate();
            break;
        }
        default: break;
    }
    // CLog::LogU("[%s:%s] Name:%s \n",__FILE__,__FUNCTION__,pPage->GetPageName().c_str());
    return OnPostCreate(pPage);
}

INT Inone_example_800x480Activity::OnStart(Page *pPage)
{
    // CLog::LogU("[%s:%s] Name:%s\n",__FILE__,__FUNCTION__,pPage->GetPageName().c_str());
    LT_CHECK_RET(pPage,ERR_INVALID);
    INT ictrlid = pPage->GetCtrlId();
    switch(ictrlid)
    {
        case ID_BUTTON_LAYOUT:
        {
            mButtonCtrl.OnPageStart();
            break;
        }
        case ID_CANVAS_LAYOUT:
        {
            mCanvasCtrl.OnPageStart();
            break;
        }
        case ID_CASE_LAYOUT:
        {
            mCaseCtrl.OnPageStart();
            break;
        }
        case ID_EDIT_LAYOUT:
        {
            mEditCtrl.OnPageStart();
            break;
        }
        case ID_LIST_LAYOUT:
        {
            mListCtrl.OnPageStart();
            break;
        }
        case ID_MAIN_LAYOUT:
        {
            mMainCtrl.OnPageStart();
            break;
        }
        case ID_METER_LAYOUT:
        {
            mMeterCtrl.OnPageStart();
            break;
        }
        case ID_PICTURE_LAYOUT:
        {
            mPictureCtrl.OnPageStart();
            break;
        }
        case ID_PROGRESS_LAYOUT:
        {
            mProgressCtrl.OnPageStart();
            break;
        }
        case ID_SETTINGS_MAIN_LAYOUT:
        {
            mSettings_mainCtrl.OnPageStart();
            break;
        }
        case ID_TEXT_LAYOUT:
        {
            mTextCtrl.OnPageStart();
            break;
        }
        case ID_TIME_LAYOUT:
        {
            mTimeCtrl.OnPageStart();
            break;
        }
        default: break;
    }
    return OnPostStart(pPage);
}

INT Inone_example_800x480Activity::OnDestroy(Page *pPage)
{
    // CLog::LogU("[%s:%s] Name:%s\n",__FILE__,__FUNCTION__,pPage->GetPageName().c_str());
    LT_CHECK_RET(pPage,ERR_INVALID);
    INT ictrlid = pPage->GetCtrlId();
    switch(ictrlid)
    {
        case ID_BUTTON_LAYOUT:
        {
            mButtonCtrl.OnPageDestroy();
            break;
        }
        case ID_CANVAS_LAYOUT:
        {
            mCanvasCtrl.OnPageDestroy();
            break;
        }
        case ID_CASE_LAYOUT:
        {
            mCaseCtrl.OnPageDestroy();
            break;
        }
        case ID_EDIT_LAYOUT:
        {
            mEditCtrl.OnPageDestroy();
            break;
        }
        case ID_LIST_LAYOUT:
        {
            mListCtrl.OnPageDestroy();
            break;
        }
        case ID_MAIN_LAYOUT:
        {
            mMainCtrl.OnPageDestroy();
            break;
        }
        case ID_METER_LAYOUT:
        {
            mMeterCtrl.OnPageDestroy();
            break;
        }
        case ID_PICTURE_LAYOUT:
        {
            mPictureCtrl.OnPageDestroy();
            break;
        }
        case ID_PROGRESS_LAYOUT:
        {
            mProgressCtrl.OnPageDestroy();
            break;
        }
        case ID_SETTINGS_MAIN_LAYOUT:
        {
            mSettings_mainCtrl.OnPageDestroy();
            break;
        }
        case ID_TEXT_LAYOUT:
        {
            mTextCtrl.OnPageDestroy();
            break;
        }
        case ID_TIME_LAYOUT:
        {
            mTimeCtrl.OnPageDestroy();
            break;
        }
        default: break;
    }
    return OnPostDestroy(pPage);
}

INT Inone_example_800x480Activity::OnStop(Page *pPage)
{
    // CLog::LogU("[%s:%s] Name:%s\n",__FILE__,__FUNCTION__,pPage->GetPageName().c_str());
    LT_CHECK_RET(pPage,ERR_INVALID);
    INT ictrlid = pPage->GetCtrlId();
    switch(ictrlid)
    {
        case ID_BUTTON_LAYOUT:
        {
            mButtonCtrl.OnPageStop();
            break;
        }
        case ID_CANVAS_LAYOUT:
        {
            mCanvasCtrl.OnPageStop();
            break;
        }
        case ID_CASE_LAYOUT:
        {
            mCaseCtrl.OnPageStop();
            break;
        }
        case ID_EDIT_LAYOUT:
        {
            mEditCtrl.OnPageStop();
            break;
        }
        case ID_LIST_LAYOUT:
        {
            mListCtrl.OnPageStop();
            break;
        }
        case ID_MAIN_LAYOUT:
        {
            mMainCtrl.OnPageStop();
            break;
        }
        case ID_METER_LAYOUT:
        {
            mMeterCtrl.OnPageStop();
            break;
        }
        case ID_PICTURE_LAYOUT:
        {
            mPictureCtrl.OnPageStop();
            break;
        }
        case ID_PROGRESS_LAYOUT:
        {
            mProgressCtrl.OnPageStop();
            break;
        }
        case ID_SETTINGS_MAIN_LAYOUT:
        {
            mSettings_mainCtrl.OnPageStop();
            break;
        }
        case ID_TEXT_LAYOUT:
        {
            mTextCtrl.OnPageStop();
            break;
        }
        case ID_TIME_LAYOUT:
        {
            mTimeCtrl.OnPageStop();
            break;
        }
        default: break;
    }
    return OnPostStop(pPage);
}

INT Inone_example_800x480Activity::OnEvent(Page *pPage, UINT iMsg, WPARAM wParam, LPARAM lParam,DWORD dwTime)
{
    LT_CHECK_RET(pPage,ERR_INVALID);
	INT iret = ERR_OK;
	INT ictrlid = pPage->GetCtrlId();
	
	// CLog::LogU("[%s:%s] iMsg:%d \n",__FILE__,__FUNCTION__,iMsg);
    switch(ictrlid)
    {
        case ID_BUTTON_LAYOUT:
        {
            if (InoIsTouchMsg(iMsg)){
                iret = mButtonCtrl.OnPageTouchEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostTouchEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            else if (InoIsKeyMsg(iMsg)){
                iret = mButtonCtrl.OnPageKeyEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostKeyEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            break;
        }
        case ID_CANVAS_LAYOUT:
        {
            if (InoIsTouchMsg(iMsg)){
                iret = mCanvasCtrl.OnPageTouchEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostTouchEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            else if (InoIsKeyMsg(iMsg)){
                iret = mCanvasCtrl.OnPageKeyEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostKeyEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            break;
        }
        case ID_CASE_LAYOUT:
        {
            if (InoIsTouchMsg(iMsg)){
                iret = mCaseCtrl.OnPageTouchEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostTouchEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            else if (InoIsKeyMsg(iMsg)){
                iret = mCaseCtrl.OnPageKeyEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostKeyEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            break;
        }
        case ID_EDIT_LAYOUT:
        {
            if (InoIsTouchMsg(iMsg)){
                iret = mEditCtrl.OnPageTouchEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostTouchEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            else if (InoIsKeyMsg(iMsg)){
                iret = mEditCtrl.OnPageKeyEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostKeyEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            break;
        }
        case ID_LIST_LAYOUT:
        {
            if (InoIsTouchMsg(iMsg)){
                iret = mListCtrl.OnPageTouchEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostTouchEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            else if (InoIsKeyMsg(iMsg)){
                iret = mListCtrl.OnPageKeyEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostKeyEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            break;
        }
        case ID_MAIN_LAYOUT:
        {
            if (InoIsTouchMsg(iMsg)){
                iret = mMainCtrl.OnPageTouchEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostTouchEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            else if (InoIsKeyMsg(iMsg)){
                iret = mMainCtrl.OnPageKeyEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostKeyEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            break;
        }
        case ID_METER_LAYOUT:
        {
            if (InoIsTouchMsg(iMsg)){
                iret = mMeterCtrl.OnPageTouchEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostTouchEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            else if (InoIsKeyMsg(iMsg)){
                iret = mMeterCtrl.OnPageKeyEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostKeyEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            break;
        }
        case ID_PICTURE_LAYOUT:
        {
            if (InoIsTouchMsg(iMsg)){
                iret = mPictureCtrl.OnPageTouchEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostTouchEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            else if (InoIsKeyMsg(iMsg)){
                iret = mPictureCtrl.OnPageKeyEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostKeyEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            break;
        }
        case ID_PROGRESS_LAYOUT:
        {
            if (InoIsTouchMsg(iMsg)){
                iret = mProgressCtrl.OnPageTouchEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostTouchEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            else if (InoIsKeyMsg(iMsg)){
                iret = mProgressCtrl.OnPageKeyEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostKeyEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            break;
        }
        case ID_SETTINGS_MAIN_LAYOUT:
        {
            if (InoIsTouchMsg(iMsg)){
                iret = mSettings_mainCtrl.OnPageTouchEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostTouchEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            else if (InoIsKeyMsg(iMsg)){
                iret = mSettings_mainCtrl.OnPageKeyEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostKeyEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            break;
        }
        case ID_TEXT_LAYOUT:
        {
            if (InoIsTouchMsg(iMsg)){
                iret = mTextCtrl.OnPageTouchEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostTouchEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            else if (InoIsKeyMsg(iMsg)){
                iret = mTextCtrl.OnPageKeyEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostKeyEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            break;
        }
        case ID_TIME_LAYOUT:
        {
            if (InoIsTouchMsg(iMsg)){
                iret = mTimeCtrl.OnPageTouchEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostTouchEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            else if (InoIsKeyMsg(iMsg)){
                iret = mTimeCtrl.OnPageKeyEvent(iMsg, wParam, lParam, dwTime);
                if (iret < ERR_OK_EVENTDONE)
                {
                    iret = OnPostKeyEvent(pPage, iMsg, wParam, lParam, dwTime);
                }
            }
            break;
        }
        default: break;
    }
    return iret;
}

VOID Inone_example_800x480Activity::OnUserTimer(LPVOID pUser,UINT32 iInterval) 
{
    LT_CHECK(pUser);
    if (pUser != this)
    {
        ((IPageCtrl *)pUser)->OnPageTimer(iInterval);
    }
    OnPostUserTimer(pUser, iInterval);
}

VOID Inone_example_800x480Activity::OnClick(InoControl *pControl, WPARAM wParam, LPARAM lParam)
{
    // CLog::LogU("[%s:%s] Control Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

VOID Inone_example_800x480Activity::OnLongClick(InoControl *pControl, WPARAM wParam, LPARAM lParam)
{
    // CLog::LogU("[%s:%s] Control Name:%s \n",__FILE__,__FUNCTION__,pControl->GetName().c_str());
}

