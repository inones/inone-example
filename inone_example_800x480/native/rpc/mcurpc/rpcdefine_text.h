/**
 *
 * NOTE: This class is auto generated by the Aiinone-IDE.
 */


#ifndef __RPCDEFINE_TEXT_H__
#define __RPCDEFINE_TEXT_H__

//text page controls
enum
{
    EPAGE_TEXT_ABOUTTITLE,
    EPAGE_TEXT_LABEL1,
    EPAGE_TEXT_LABEL2,
    EPAGE_TEXT_LABEL3,
    EPAGE_TEXT_LABEL4,
    EPAGE_TEXT_LABEL5,
    EPAGE_TEXT_LABEL6,
    EPAGE_TEXT_LABEL7,
    EPAGE_TEXT_LABEL8,
};

#endif //__RPCDEFINE_TEXT_H__
