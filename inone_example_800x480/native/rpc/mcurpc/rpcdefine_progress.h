/**
 *
 * NOTE: This class is auto generated by the Aiinone-IDE.
 */


#ifndef __RPCDEFINE_PROGRESS_H__
#define __RPCDEFINE_PROGRESS_H__

//progress page controls
enum
{
    EPAGE_PROGRESS_SEEKBAR1,
    EPAGE_PROGRESS_CIRCLEBAR1,
    EPAGE_PROGRESS_CIRCLEBAR2,
    EPAGE_PROGRESS_SEEKBAR2,
    EPAGE_PROGRESS_CIRCLEBAR3,
    EPAGE_PROGRESS_LABEL1,
    EPAGE_PROGRESS_LABEL2,
    EPAGE_PROGRESS_LABEL3,
};

#endif //__RPCDEFINE_PROGRESS_H__
