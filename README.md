# inone-example

#### 介绍
inone-example为合一智联的APP演示样机参考代码，主要针对ino8a平台。

#### 软件架构
软件架构说明


#### 安装教程

1.  安装Aiinone-IDE工具，下载路径：https://www.aiinone.cn/developer.html
2.  安装inone-sdk，git clone https://gitee.com/inones/inone-example.git
    默认sdk安装路径，inone-sdk与aiinoneide同一级目录，如需要调整SDK路径需要修改windows环境变量INONE-SDK的路径指向。
    ![输入图片说明](https://foruda.gitee.com/images/1661757360078786607/38bef32a_9238538.png "屏幕截图")
3.  下载inone-example，git clone https://gitee.com/inones/inone-example.git

#### 使用说明

1.  确保上面的Aiinone-IDE和inone-sdk成功安装配置；
2.  点击启动Aiinone-IDE，选择打开工作组（选择inone_example.code-workspace）或者选择打开文件夹（选择inone_example_1024x600等文件夹名）。
3.  编译代码CTRL+ALT+C
4.  下载到平台机器即可看到效果


#### 特技


- 使用aiinone的SDK包可以提供完备的GUI开发，多达30种控件；
- Aiinone-IDE + SDK搭配使用，可以提供优秀的嵌入式开发环境，让用户嵌入式系统开发更加得心应手；
- Aiinone-IDE + SDK搭配使用，智能化布局 简单快捷；
- Aiinone-IDE + SDK搭配使用，智能化编程 省时省心；
- Aiinone-IDE + SDK搭配使用，成果呈现 说见就见;


